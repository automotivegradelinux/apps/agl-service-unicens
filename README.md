# UNICENS Service

## Overview
This service runs the "UNICENS Managament Library" which is responsible
to start an INICnet network and configure well-known nodes and create
audio/video routes.
The respective network setup is described in on an XML file which is
either automatically loaded after starting the service, or later on
demand.

## Verbs

| Name               | Description                                   | JSON Parameters                                                   |
|--------------------|-----------------------------------------------|-------------------------------------------------------------------|
| listconfig         | List configuration files in a given directory.| *Request:* {"cfgpath": "/path/to/directory"}                      |
| initialise         | Initialize the service by loading a specific configuration. | *Request:* {"filename": "/path/to/file.xml"}        |
| subscribe          | Subscribe to "node-availibility" event.       | *Request:* { }                                                    |
| subscriberx        | Subscribe to "rx-message" event.              | *Request:* { }                                                    |
| sendmessage        | Transmit a control message to a node.         | *Request:* {"node" : 624, "msgid" : 123, "data" : "AAA="}         |
| writei2c           | Write I2C data to a node.                     | *Request:* {"node" : 624, "data" : [7,3,255]}                     |

Please note that it may not be required to call the "initialze" verb. The binding call automatically "initialize" when it is started.
It automatically loads the configuration "data/config_multichannel_audio_kit.xml". This required that a compatible INICnet hardware is
connected and the respective driver is initialized.

## Events
| Name               | Description                                   | JSON Parameters                                                   |
|--------------------|-----------------------------------------------|-------------------------------------------------------------------|
| node-availibility  | Availability of a node has changed.           | *Response:* {"node" : 624, "available" : true}                    |
| rx-message         | Received a message from a node.               | *Response:* {"node" : 624, "msgid" : 123, "data" : "AAA="}        |


## Submodules
This service depends on the UNICENS Management Library which is published on GitHub.
See https://github.com/MicrochipTech/unicens for further information.

## Hardware
AGL images are pre-configured to run with Microchip Slim Board Family. For furter information see https://github.com/MicrochipTech/unicens ,
section "Hardware".

# Build and Run on Standard Linux Distributions

## Cloning Audio-Binding from Gerrit

```
git clone --recurse-submodules git clone https://gerrit.automotivelinux.org/gerrit/apps/agl-service-unicens
cd  agl-service-unicens
```

## Install AFB-Daemon from OBS
See chapter "Host Configuration" in AGL developers documentation.

[Prerequisites](http://docs.automotivelinux.org/master/docs/devguides/en/dev/reference/host-configuration/docs/1_Prerequisites.html)

[AGL Application Framework](http://docs.automotivelinux.org/master/docs/devguides/en/dev/reference/host-configuration/docs/2_AGL_Application_Framework.html)

# Compile binding

```
source ~/.bashrc  # or any other file where your have place your compilation preferences
mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX ..
make

afb-daemon --workdir=.. --ldpaths=build --port=1234  --roothttp=./htdocs --token="" --verbose

speaker-test -twav -D hw:ep01 -c 2
firefox http://localhost:1234
```

# Archive

```
VERSION=0.1
GIT_TAG=master
PKG_NAME=UNICENS-agent
git archive --format=tar.gz --prefix=${PKG_NAME}-${VERSION}/ ${GIT_TAG} -o ${PKG_NAME}_${VERSION}.orig.tar.gz
```

# Local Source Debug with GDB

Warning: technically AGL bindings are shared libraries loaded thought 'dlopen'. GDB supports source debug of dynamically
loaded libraries, but user should be warn that the actual path to sharelib symbols is directly inherited from DLOPEN.
As a result if you change your directory after binder start with --workdir=xxx then GDB will not find symbols anymore

```
Examples:

# WORK when running in direct
afb-daemon --workdir=.. --ldpaths=build --port=1234 --roothttp=./htdocs

# FAIL when using GDB with warning: Could not load shared library ....
gdb -args afb-daemon --workdir=.. --ldpaths=build --port=1234 --roothttp=./htdocs
...
warning: Could not load shared library symbols for ./build/ucs2-afb/afb-ucs2.so.
Do you need "set solib-search-path" or "set sysroot"?
    ...
```

To debug sharelib symbol path: start your binder under GDB. Then break your session after the binder has
loaded its bindings. Finally use "info sharedlibrary" and check 'SymsRead'. If equal to 'No' then either you start GDB
from the wrong relative directory, either you have to use 'set solib-search-path' to force the path.

```
gdb -args afb-daemon --workdir=.. --ldpaths=build --port=1234 --roothttp=./htdocs
run
    ...
    NOTICE: API UNICENS added
    NOTICE: Waiting port=1234 rootdir=.
    NOTICE: Browser URL= http://localhost:1234
(hit Ctrl-C to break the execution)
info sharedlibrary afb-*
```

# Running an debugging on a target

```
export RSYNC_TARGET=root@xx.xx.xx.xx
export RSYNC_PREFIX=/opt    # WARNING: installation directory should exist

mkdir $RSYNC_TARGET
cd    $RSYNC_TARGET

cmake -DRSYNC_TARGET=$RSYNC_TARGET -DRSYNC_PREFIX=$RSYNC_PREFIX ..
make remote-target-populate

    ./build/target/start-${RSYNC_TARGET}.sh
    firefox http://localhost:1234    # WARNING: do not forget firewall if any

    ssh -tt ${RSYNC_TARGET} speaker-test -twav -D hw:ep01 -c 2
```

Note: remote-target-populate will
 - create a script to remotely start the binder on the target in ./build/target/start-on-target-name.sh
 - create a gdbinit file to transparently debug remotely in source code with gdb -x ./build/target/gdb-on-target-name.ini
 - to run and debug directly from your IDE just configure the run and debug properties with the corresponding filename

Note that Netbeans impose to set debug directory to ./build/pkgout or it won't find binding symbols for source debugging

# Default Volume of Slim Amplifiers
The binding currently supports two use cases for amplifiers.
1. Amplifiers are initialized with a default volume. The head unit uses software volume
   to change the volume of streaming data.
2. Amplifiers are initialized muted. The head unit uses hardware volume (e.g. HAL-MOST-UNICENS)
   to change the volume of amplifiers remotely.

Use case 2 is the default use case. If you like to use this binding without hardware volume support
please adopt the ```config_multichannel_audio_kit.xml``` as explained below.

```
<!-- Register 7: Master Volume (Max Volume=07 00 00 and Min Volume=07 03 FF) -->
<!--   - together with HAL-MOST-UNICENS binding use "07 03 FF" = muted after start -->
<!--   - otherwise use "07 01 50" = default volume -->
<I2CPortWrite Address="0x2A" Payload="07 03 FF"/>
```
