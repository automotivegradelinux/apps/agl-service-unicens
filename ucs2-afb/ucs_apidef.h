
static const char _afb_description_UNICENS[] =
	"{\"openapi\":\"3.0.0\",\"$schema\":\"http:iot.bzh/download/openapi/s"
	"chema-3.0/default-schema.json\",\"info\":{\"description\":\"\",\"tit"
	"le\":\"ucs2\",\"version\":\"1.0\",\"x-binding-c-generator\":{\"api\""
	":\"UNICENS\",\"version\":3,\"prefix\":\"ucs2_\",\"postfix\":\"\",\"s"
	"tart\":null,\"onevent\":null,\"preinit\":null,\"init\":\"ucs2_initbi"
	"nding\",\"scope\":\"\",\"private\":false}},\"servers\":[{\"url\":\"w"
	"s://{host}:{port}/api/monitor\",\"description\":\"Unicens2 API.\",\""
	"variables\":{\"host\":{\"default\":\"localhost\"},\"port\":{\"defaul"
	"t\":\"1234\"}},\"x-afb-events\":[{\"$ref\":\"#/components/schemas/af"
	"b-event\"}]}],\"components\":{\"schemas\":{\"afb-reply\":{\"$ref\":\""
	"#/components/schemas/afb-reply-v2\"},\"afb-event\":{\"$ref\":\"#/com"
	"ponents/schemas/afb-event-v2\"},\"afb-reply-v2\":{\"title\":\"Generi"
	"c response.\",\"type\":\"object\",\"required\":[\"jtype\",\"request\""
	"],\"properties\":{\"jtype\":{\"type\":\"string\",\"const\":\"afb-rep"
	"ly\"},\"request\":{\"type\":\"object\",\"required\":[\"status\"],\"p"
	"roperties\":{\"status\":{\"type\":\"string\"},\"info\":{\"type\":\"s"
	"tring\"},\"token\":{\"type\":\"string\"},\"uuid\":{\"type\":\"string"
	"\"},\"reqid\":{\"type\":\"string\"}}},\"response\":{\"type\":\"objec"
	"t\"}}},\"afb-event-v2\":{\"type\":\"object\",\"required\":[\"jtype\""
	",\"event\"],\"properties\":{\"jtype\":{\"type\":\"string\",\"const\""
	":\"afb-event\"},\"event\":{\"type\":\"string\"},\"data\":{\"type\":\""
	"object\"}}}},\"x-permissions\":{\"config\":{\"permission\":\"urn:AGL"
	":permission:UNICENS:public:initialise\"},\"monitor\":{\"permission\""
	":\"urn:AGL:permission:UNICENS:public:monitor\"},\"controller\":{\"pe"
	"rmission\":\"urn:AGL:permission:UNICENS:public:controller\"}},\"resp"
	"onses\":{\"200\":{\"description\":\"A complex object array response\""
	",\"content\":{\"application/json\":{\"schema\":{\"$ref\":\"#/compone"
	"nts/schemas/afb-reply\"}}}}}},\"paths\":{\"/listconfig\":{\"descript"
	"ion\":\"List Config Files\",\"get\":{\"x-permissions\":{\"$ref\":\"#"
	"/components/x-permissions/config\"},\"parameters\":[{\"in\":\"query\""
	",\"name\":\"cfgpath\",\"required\":false,\"schema\":{\"type\":\"stri"
	"ng\"}}],\"responses\":{\"200\":{\"$ref\":\"#/components/responses/20"
	"0\"}}}},\"/initialise\":{\"description\":\"configure Unicens2 lib fr"
	"om NetworkConfig.XML.\",\"get\":{\"x-permissions\":{\"$ref\":\"#/com"
	"ponents/x-permissions/config\"},\"parameters\":[{\"in\":\"query\",\""
	"name\":\"filename\",\"required\":true,\"schema\":{\"type\":\"string\""
	"}}],\"responses\":{\"200\":{\"$ref\":\"#/components/responses/200\"}"
	"}}},\"/subscribe\":{\"description\":\"Subscribe to network events.\""
	",\"get\":{\"x-permissions\":{\"$ref\":\"#/components/x-permissions/m"
	"onitor\"},\"responses\":{\"200\":{\"$ref\":\"#/components/responses/"
	"200\"}}}},\"/subscriberx\":{\"description\":\"Subscribe to Rx contro"
	"l message events.\",\"get\":{\"x-permissions\":{\"$ref\":\"#/compone"
	"nts/x-permissions/monitor\"},\"responses\":{\"200\":{\"$ref\":\"#/co"
	"mponents/responses/200\"}}}},\"/writei2c\":{\"description\":\"Writes"
	" I2C command to remote node.\",\"get\":{\"x-permissions\":{\"$ref\":"
	"\"#/components/x-permissions/monitor\"},\"parameters\":[{\"in\":\"qu"
	"ery\",\"name\":\"node\",\"required\":true,\"schema\":{\"type\":\"int"
	"eger\",\"format\":\"int32\"}},{\"in\":\"query\",\"name\":\"data\",\""
	"required\":true,\"schema\":{\"type\":\"array\",\"format\":\"int32\"}"
	",\"style\":\"simple\"}],\"responses\":{\"200\":{\"$ref\":\"#/compone"
	"nts/responses/200\"}}}},\"/sendmessage\":{\"description\":\"Transmit"
	"s a control message to a node.\",\"get\":{\"x-permissions\":{\"$ref\""
	":\"#/components/x-permissions/controller\"},\"parameters\":[{\"in\":"
	"\"query\",\"name\":\"node\",\"required\":true,\"schema\":{\"type\":\""
	"integer\",\"format\":\"int32\"}},{\"in\":\"query\",\"name\":\"msgid\""
	",\"required\":true,\"schema\":{\"type\":\"integer\",\"format\":\"int"
	"32\"}},{\"in\":\"query\",\"name\":\"data\",\"required\":false,\"sche"
	"ma\":{\"type\":\"string\",\"format\":\"byte\"},\"style\":\"simple\"}"
	"],\"responses\":{\"200\":{\"$ref\":\"#/components/responses/200\"}}}"
	"}}}"
;

static const struct afb_auth _afb_auths_UNICENS[] = {
	{ .type = afb_auth_Permission, .text = "urn:AGL:permission:UNICENS:public:initialise" },
	{ .type = afb_auth_Permission, .text = "urn:AGL:permission:UNICENS:public:monitor" },
	{ .type = afb_auth_Permission, .text = "urn:AGL:permission:UNICENS:public:controller" }
};

void ucs2_listconfig(afb_req_t req);
void ucs2_initialise(afb_req_t req);
void ucs2_subscribe(afb_req_t req);
void ucs2_subscriberx(afb_req_t req);
void ucs2_writei2c(afb_req_t req);
void ucs2_sendmessage(afb_req_t req);

static const struct afb_verb_v3 _afb_verbs_UNICENS[] = {
    {
        .verb = "listconfig",
        .callback = ucs2_listconfig,
        .auth = &_afb_auths_UNICENS[0],
        .info = "List Config Files",
        .vcbdata = NULL,
        .session = AFB_SESSION_NONE,
        .glob = 0
    },
    {
        .verb = "initialise",
        .callback = ucs2_initialise,
        .auth = &_afb_auths_UNICENS[0],
        .info = "configure Unicens2 lib from NetworkConfig.XML.",
        .vcbdata = NULL,
        .session = AFB_SESSION_NONE,
        .glob = 0
    },
    {
        .verb = "subscribe",
        .callback = ucs2_subscribe,
        .auth = &_afb_auths_UNICENS[1],
        .info = "Subscribe to network events.",
        .vcbdata = NULL,
        .session = AFB_SESSION_NONE,
        .glob = 0
    },
    {
        .verb = "subscriberx",
        .callback = ucs2_subscriberx,
        .auth = &_afb_auths_UNICENS[1],
        .info = "Subscribe to Rx control message events.",
        .vcbdata = NULL,
        .session = AFB_SESSION_NONE,
        .glob = 0
    },
    {
        .verb = "writei2c",
        .callback = ucs2_writei2c,
        .auth = &_afb_auths_UNICENS[1],
        .info = "Writes I2C command to remote node.",
        .vcbdata = NULL,
        .session = AFB_SESSION_NONE,
        .glob = 0
    },
    {
        .verb = "sendmessage",
        .callback = ucs2_sendmessage,
        .auth = &_afb_auths_UNICENS[2],
        .info = "Transmits a control message to a node.",
        .vcbdata = NULL,
        .session = AFB_SESSION_NONE,
        .glob = 0
    },
    {
        .verb = NULL,
        .callback = NULL,
        .auth = NULL,
        .info = NULL,
        .vcbdata = NULL,
        .session = 0,
        .glob = 0
    }
};

int ucs2_initbinding(afb_api_t api);

const struct afb_binding_v3 afbBindingV3 = {
    .api = "UNICENS",
    .specification = _afb_description_UNICENS,
    .info = "",
    .verbs = _afb_verbs_UNICENS,
    .preinit = NULL,
    .init = ucs2_initbinding,
    .onevent = NULL,
    .userdata = NULL,
    .provide_class = NULL,
    .require_class = NULL,
    .require_api = NULL,
    .noconcurrency = 0
};

